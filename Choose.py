import os
import sys
from PyQt5.QtGui import *
from PyQt5.QtWidgets import *
from PyQt5.QtCore import *
# from App import App
from Camera import Camera
from Sensor import Sensor
from function import icon_path


class Choose(QMainWindow):
    def __init__(self, master=None):
        QMainWindow.__init__(self, master)
        self.setWindowTitle("机器人视频流图传系统")
        self.setWindowIcon(QIcon(os.path.join(icon_path, "cam.ico")))
        # self.setFixedSize(460, 200)
        self.setFixedSize(360, 180)
        self.center()
        self.cam_active = False
        self.sen_active = False
        self.createUI()
        self.createSignalSlot()

    def center(self):
        screen = QDesktopWidget()
        size = self.geometry()
        self.move((screen.width() - size.width()) / 2, (screen.height() - size.height()) / 2)

    def createUI(self):
        self.widget = QWidget(self)
        self.setCentralWidget(self.widget)
        self.widget.setAutoFillBackground(True)
        self.title = QLabel("请选择模块")
        # font
        self.font1 = QFont()
        self.font1.setFamily("思源宋体 Heavy")
        self.font1.setPointSize(18)
        self.font1.setBold(True)
        self.font1.setWeight(75)
        self.font2 = QFont()
        self.font2.setFamily("思源黑体 Medium")
        self.font2.setPointSize(12)
        # title
        self.title.setFont(self.font1)
        self.title.setAlignment(Qt.AlignCenter)
        # btn
        self.cam_btn = QPushButton("摄像头")
        self.sen_btn = QPushButton("激光传感器")
        self.cam_btn.setFont(self.font2)
        self.sen_btn.setFont(self.font2)
        # layout
        vbox = QVBoxLayout()
        vbox.addWidget(self.title)
        vbox.addWidget(self.cam_btn)
        vbox.addWidget(self.sen_btn)
        self.widget.setLayout(vbox)

    def createSignalSlot(self):
        self.cam_btn.clicked.connect(self.goCam)
        self.sen_btn.clicked.connect(self.goSen)
    
    def goCam(self):
        self.cam = Camera()
        self.cam.showMaximized()
        # 手动输入url
        # input = QInputDialog(self)
        # input.setFixedSize(460, 120)
        # input.setInputMode(QInputDialog.TextInput)
        # input.setFont(self.font2)
        # input.setWindowTitle('输入')
        # input.setLabelText('请输入监控视频流URL:')
        # input.setTextValue("rtsp://admin:1qa2ws3ed@192.168.88.111:554")
        # url = input.textValue()
        # ok = input.exec_()
        # if ok:
        #     win.getUrl(url)
        # else:
        #     QMessageBox.warning(self, '警告', '获取视频流失败', QMessageBox.Yes)
        # 固定url
        self.cam.getUrl('rtsp://admin:1qa2ws3ed@192.168.88.111:554')
        # self.close()
        self.cam_active = True
        self.cam_btn.setDisabled(True)
        if self.cam_active and self.sen_active:
            self.close()


    def goSen(self):
        self.sen = Sensor()
        self.sen.showMaximized()
        # self.close()
        self.sen_active = True
        self.sen_btn.setDisabled(True)
        if self.cam_active and self.sen_active:
            self.close()


if __name__ == '__main__':
    app = QApplication(sys.argv)
    cs = Choose()
    cs.show()
    sys.exit(app.exec_())
