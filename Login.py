# -*- coding: utf-8 -*- 
#
# Created by Xinyu on 2020/8/1 23:39.
#

import os
import sys
from PyQt5.QtGui import *
from PyQt5.QtWidgets import *
from PyQt5.QtCore import *
# from App import App
from Choose import Choose
from function import icon_path


class Login(QMainWindow):
    def __init__(self, master=None):
        QMainWindow.__init__(self, master)
        self.setWindowTitle("机器人视频流图传系统")
        self.setWindowIcon(QIcon(os.path.join(icon_path, "cam.ico")))
        # self.setFixedSize(460, 200)
        self.setFixedSize(360, 180)
        self.center()
        self.createUI()
        self.createSignalSlot()

    def center(self):
        screen = QDesktopWidget()
        size = self.geometry()
        self.move((screen.width() - size.width()) / 2, (screen.height() - size.height()) / 2)

    def createUI(self):
        self.widget = QWidget(self)
        self.setCentralWidget(self.widget)
        self.widget.setAutoFillBackground(True)
        self.title = QLabel("机器人视频流图传系统")
        # font
        self.font1 = QFont()
        self.font1.setFamily("思源宋体 Heavy")
        self.font1.setPointSize(22)
        self.font1.setBold(True)
        self.font1.setWeight(75)
        self.font2 = QFont()
        self.font2.setFamily("思源黑体 Medium")
        self.font2.setPointSize(12)
        # title
        self.title.setFont(self.font1)
        self.title.setAlignment(Qt.AlignCenter)
        # input
        self.label1 = QLabel("账号")
        self.label1.setFixedHeight(30)
        self.label1.setFont(self.font2)
        self.username = QLineEdit("")
        self.username.setFont(self.font2)
        self.username.setPlaceholderText("请输入账号")
        self.username.setFixedHeight(30)
        self.username.setText("admin")
        self.label2 = QLabel("密码")
        self.label2.setFixedHeight(30)
        self.label2.setFont(self.font2)
        self.password = QLineEdit("")
        self.password.setEchoMode(QLineEdit.Password)
        self.password.setFont(self.font2)
        self.password.setPlaceholderText("请输入密码")
        self.password.setFixedHeight(30)
        # self.password.setText("123456")
        # self.label3 = QLabel("url")
        # self.label3.setFixedHeight(30)
        # self.label3.setFont(self.font2)
        # self.url = QLineEdit("")
        # self.url.setFont(self.font2)
        # self.url.setPlaceholderText("请输入url")
        # self.url.setFixedHeight(30)
        # self.url.setText('rtsp://admin:1qa2ws3ed@192.168.88.111:554')
        # btn
        self.login_btn = QPushButton("确定")
        self.cancel_btn = QPushButton("取消")
        self.login_btn.setFont(self.font2)
        self.cancel_btn.setFont(self.font2)
        # layout
        form = QFormLayout()
        form.setAlignment(Qt.AlignCenter)
        form.addRow(self.label1, self.username)
        form.addRow(self.label2, self.password)
        # form.addRow(self.label3, self.url)
        hbox = QHBoxLayout()
        hbox.addWidget(self.login_btn)
        hbox.addWidget(self.cancel_btn)
        vbox = QVBoxLayout()
        vbox.addStretch(0)
        vbox.addWidget(self.title)
        vbox.addLayout(form)
        vbox.addLayout(hbox)
        self.widget.setLayout(vbox)

    def createSignalSlot(self):
        self.login_btn.clicked.connect(self.loginOK)
        self.cancel_btn.clicked.connect(self.loginCancel)

    def loginOK(self):
        username = self.username.text()
        password = self.password.text()
        # url = self.url.text()
        # 固定用户名和密码
        if username == 'admin' and password == '123456':
            # login.close()
            self.win = Choose()
            self.win.show()
            # win = App()
            # win.showMaximized()
            infobox = QMessageBox()
            infobox.setIcon(QMessageBox.Information)
            infobox.setText('登录成功！')
            infobox.setWindowTitle('成功')
            infobox.setStandardButtons(QMessageBox.Ok)
            infobox.button(QMessageBox.Ok).animateClick(3 * 1000)
            infobox.exec_()
            # 手动输入url
            # input = QInputDialog(self)
            # input.setFixedSize(460, 120)
            # input.setInputMode(QInputDialog.TextInput)
            # input.setFont(self.font2)
            # input.setWindowTitle('输入')
            # input.setLabelText('请输入监控视频流URL:')
            # input.setTextValue("rtsp://admin:1qa2ws3ed@192.168.88.111:554")
            # url = input.textValue()
            # ok = input.exec_()
            # if ok:
            #     win.getUrl(url)
            # else:
            #     QMessageBox.warning(self, '警告', '获取视频流失败', QMessageBox.Yes)
            # 固定url
            # win.getUrl('rtsp://admin:1qa2ws3ed@192.168.88.111:554')
            login.close()
        else:
            QMessageBox.warning(self, '警告', '用户名或者密码错误!', QMessageBox.Yes)
            self.username.setFocus()

    @staticmethod
    def loginCancel():
        login.close()

    def keyPressEvent(self, event):
        if event.key() == Qt.Key_Enter:
            self.loginOK()
        elif event.key() == Qt.Key_Escape:
            self.loginCancel()


if __name__ == '__main__':
    app = QApplication(sys.argv)
    login = Login()
    login.show()
    sys.exit(app.exec_())
