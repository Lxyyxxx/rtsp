# -*- coding: utf-8 -*- 
#
# Created by Xinyu on 2020/8/1 23:02.
#

import os
import sys
from PyQt5.QtWidgets import *
from PyQt5.QtGui import *
from PyQt5.QtCore import *
# import qtawesome as qta
from RealTimeMonitor import RealTimeMonitor
from ShowCapture import ShowCapture
# from CollectData import CollectData
from DrawData import CollectData
from ShowData import ShowData
from ShowVideo import ShowVideo
from function import refresh, icon_path


class Camera(QMainWindow):
    def __init__(self, master=None):
        QMainWindow.__init__(self, master)
        self.master = master
        self.url = None
        self.setWindowTitle("机器人视频流图传系统")
        self.setWindowIcon(QIcon(os.path.join(icon_path, "cam.ico")))
        self.tab = QTabWidget(self)
        self.setCentralWidget(self.tab)
        self.statusbar = self.statusBar()
        self.first_click_tab2 = True
        self.createUI()
        self.createSignalSlot()

    def getUrl(self, url):
        self.url = url
        self.tab0.open(self.url)

    def createUI(self):
        # Tab0
        self.tab0 = RealTimeMonitor(self)
        self.tab.addTab(self.tab0, '视频监控')
        # if self.url:
        #     self.tab0.open(self.url)
        # Tab1
        self.tab1 = ShowCapture(self)
        self.tab.addTab(self.tab1, '截图浏览')
        # Tab2
        self.tab2 = ShowVideo(self)
        self.tab.addTab(self.tab2, "监控回放")
        # status bar
        self.statusbar.showMessage('这是状态提示栏')

    def createSignalSlot(self):
        self.tab.currentChanged.connect(self.tabClickChange)

    def tab2Hint(self):
        if self.first_click_tab2:
            # self.first_click_tab2 = False
            infobox = QMessageBox()
            infobox.setIcon(QMessageBox.Information)
            infobox.setText('请选择一个视频双击播放')
            self.statusbar.showMessage("请选择一个视频双击播放，若列表为空，则添加视频")
            infobox.setWindowTitle('选择视频')
            infobox.setStandardButtons(QMessageBox.Ok)
            infobox.button(QMessageBox.Ok).animateClick(3 * 1000)
            infobox.exec_()

    def tabClickChange(self):
        refresh()
        tab_id = self.tab.currentIndex()
        print('tab_id', tab_id)
        if tab_id == 1:
            self.tab1.getCapture()
        elif tab_id == 2:
            self.tab2Hint()
            self.tab2.getVideoList()

    def closeEvent(self, event):
        self.tab0.player.release()
        self.tab2.player.release()
        QCoreApplication.instance().quit()
        # sys.exit(app.exec_())


if __name__ == '__main__':
    app = QApplication(sys.argv)
    win = Camera()
    win.showMaximized()
    sys.exit(app.exec_())
