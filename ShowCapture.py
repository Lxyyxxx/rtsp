# -*- coding: utf-8 -*- 
#
# Created by Xinyu on 2020/8/1 23:05.
#

import os
from PyQt5.QtCore import *
from PyQt5.QtGui import *
from PyQt5.QtWidgets import *
from function import capture_path, refresh


class ShowCapture(QWidget):
    """
    截图浏览Tab
    显示文件夹中的截图，可单张展示
    """

    def __init__(self, master=None):
        QWidget.__init__(self, master)
        self.master = master
        self.createUI()
        self.createSignalShot()

    def createUI(self):
        # 存所有截图的文件名
        self.img_list = []
        # 显示所有截图
        self.list = QListWidget()
        self.list.setIconSize(QSize(240, 160))
        self.list.setResizeMode(QListWidget.Adjust)
        self.list.setViewMode(QListWidget.IconMode)
        self.list.setMovement(QListWidget.Static)
        self.list.setSpacing(8)
        # 显示单张截图
        # self.show = QLabel()
        # self.show.setAlignment(Qt.AlignCenter)
        # self.show = QGraphicsView()
        # 布局
        vbox = QVBoxLayout()
        vbox.addWidget(self.list)
        vbox.setAlignment(Qt.AlignLeft)
        self.setLayout(vbox)

    def createSignalShot(self):
        self.list.itemClicked.connect(self.maximizeCapture)

    def getCapture(self):
        """
        获得文件夹内所有截图并显示在ListWidget中
        :return:
        """
        pic_dir = QDir(capture_path)
        pic_dir.setFilter(QDir.Files | QDir.NoSymLinks)
        filters = ["*.png", "*.jpg"]
        pic_dir.setNameFilters(filters)
        self.img_list = pic_dir.entryList()
        self.list.clear()
        for i in range(len(self.img_list)):
            img_name = self.img_list[i]
            # print(img_name)
            pixmap = QPixmap(os.path.join(capture_path, img_name))
            list_item = QListWidgetItem(QIcon(pixmap.scaled(240, 160)), img_name)
            list_item.setSizeHint(QSize(240, 190))
            self.list.insertItem(i, list_item)
            refresh()

    def maximizeCapture(self, item):
        """
        放大单张截图
        :param item:
        :return:
        """
        pix = QPixmap(os.path.join(capture_path, self.img_list[self.list.row(item)]))
        # screen = QGraphicsScene()
        # screen.addPixmap(pix)
        # self.show.setScene(screen)
        # self.show.resize(pix.width(), pix.height())
        # self.show.show()
        show = Image(self, pix)
        show.show()


class Image(QMainWindow):
    def __init__(self, master=None, img=None):
        QWidget.__init__(self, master)
        self.master = master
        self.setWindowTitle('监控截图')
        self.setFixedSize(1280, 720)
        self.control = ImageControl(self, img)
        self.setCentralWidget(self.control)
        self.center()

    def center(self):
        screen = QDesktopWidget()
        size = self.geometry()
        self.move((screen.width() - size.width()) / 2, (screen.height() - size.height()) / 2)


class ImageControl(QWidget):
    def __init__(self, master=None, img=None):
        QWidget.__init__(self, master)
        self.master = master
        self.img = img
        self.factor = self.img.height() / self.img.width()
        w, h = self.getSizeWithWidth(self.width())
        self.scaled_img = self.img.scaled(w, h)
        self.point = QPoint(0, 0)

    def getSizeWithWidth(self, w):
        # 根据宽更改比例
        h = w * self.factor
        return w, h

    def paintEvent(self, event):
        # 绘制图像
        painter = QPainter()
        painter.begin(self)
        painter.drawPixmap(self.point, self.scaled_img)
        painter.end()

    def mouseMoveEvent(self, event):
        if self.left_click:
            self.end_pos = event.pos() - self.start_pos
            self.point = self.point + self.end_pos
            self.start_pos = event.pos()
            self.repaint()

    def mousePressEvent(self, event):
        if event.button() == Qt.LeftButton:
            self.left_click = True
            self.start_pos = event.pos()

    def mouseReleaseEvent(self, event):
        if event.button() == Qt.LeftButton:
            self.left_click = False
        elif event.button() == Qt.RightButton:
            # 右键单击还原
            self.point = QPoint(0, 0)
            self.scaled_img = self.img.scaled(self.size())
            self.repaint()

    def wheelEvent(self, event):
        if event.angleDelta().y() > 0:
            # 放大
            w, h = self.getSizeWithWidth(self.scaled_img.width() + 10)
            self.scaled_img = self.img.scaled(w, h)
            self.repaint()
        elif event.angleDelta().y() < 0:
            # 缩小
            w, h = self.getSizeWithWidth(self.scaled_img.width() - 10)
            self.scaled_img = self.img.scaled(w, h)
            self.repaint()

    def resizeEvent(self, event):
        if self.master:
            w, h = self.getSizeWithWidth(self.width())
            self.scaled_img = self.img.scaled(w, h)
            self.point = QPoint(0, 0)
            self.update()
