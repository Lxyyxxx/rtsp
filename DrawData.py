import sqlite3
from PyQt5.QtCore import *
from PyQt5.QtWidgets import *
from PyQt5.QtWebEngineWidgets import *
import socket
from function import refresh, getCurrentTime, addr, buffer_size, data_db_path, max_size, pi, r, radius, l_angle, r_angle, epos
from time import sleep
import numpy as np
from math import *
import plotly.graph_objects as go
import plotly.offline as po
from plotly.subplots import make_subplots
import random


class BackendCollectData(QThread):
    """
    后台收集数据
    """

    # 信号槽
    update_time = pyqtSignal(str)
    # update_height = pyqtSignal(str)
    # update_luminance = pyqtSignal(str)
    update_html = pyqtSignal(str)
    update_area_dif = pyqtSignal(str)
    update_area_res = pyqtSignal(str)
    update_height_min = pyqtSignal(str)
    update_height_avg = pyqtSignal(str)
    update_height_max = pyqtSignal(str)

    # 数据库
    conn = None
    cur = None

    def __init__(self, master=None):
        QThread.__init__(self, master)
        self.master = master
        self.area_ideal = 0.5 * pi * r * r - 0.25 * r * r * \
            sin(2*radius(l_angle)) - 0.25 * r * \
            r * sin(2*radius(180 - r_angle))

    def run(self):
        self.master.collecting = True
        try:
            self.master.master.statusbar.showMessage('正在等待socket连接')
            print('等待连接')
            client, _ = self.master.socket.accept()
            # 连接数据库，不存在会自行创建
            self.conn = sqlite3.connect(data_db_path)
            self.cur = self.conn.cursor()
            self.master.master.statusbar.showMessage('socket连接成功')
            print('连接成功')
            """
            开始切割数据
            h_offset l_offset flag state
            -1       -1       0     全是高度
            -1       +        0     l_offset前全为高度，后全为亮度
            -1       -1       1     全是亮度
            -1       +        1     x
            +        -1       0     x
            +        -1       1     h_offset前全为亮度，||，后全为高度
            +        +        0     l_offset前全为高度，l_offset到h_offset间全为亮度，||，h_offset后全为高度   hLlHl
            +        +        0     ||，h_offset到l_offset间全为高度，l_offset后全为亮度   HhLl
            +        +        1     h_offset前全为亮度，||，h_offset到l_offset间全为高度，l_offset后全为亮度   lHhLl
            +        +        1     l_offset到h_offset间全为亮度，||，h_offset后全为高度   LlHh
            """
            flag = False  # 0-高度还没读完，1-高度已读完
            self.h_val, self.l_val = "", ""
            while self.master.collecting:
                print('running...')
                refresh()
                data = client.recv(buffer_size).decode()
                # print(data)
                if not data:
                    # 更新最后一组数据
                    self.update()
                    # 提交事务，关闭数据库
                    self.cur.close()
                    self.conn.commit()
                    self.conn.close()
                    print('waiting...')
                    # 当data为空，说明客户端已断开连接，需要等待下次连接
                    client, _ = self.master.socket.accept()
                    # 重新连接数据库
                    self.conn = sqlite3.connect(data_db_path)
                    self.cur = self.conn.cursor()
                    continue
                # 找高度
                h_offset = data.find('H')
                # 找亮度
                l_offset = data.find('L')
                print(h_offset, l_offset, flag)
                if h_offset == -1:
                    if l_offset == -1:
                        # -1,-1,0
                        if not flag:
                            self.h_val += data
                        # -1,-1,1
                        else:
                            self.l_val += data
                    else:
                        # -1,+
                        self.h_val += data[:l_offset]
                        flag = True
                        self.l_val = ""
                        self.l_val += data[l_offset + 1:]
                else:
                    if l_offset == -1:
                        # +,-1
                        self.l_val += data[:h_offset]
                        if self.h_val and self.l_val:
                            print('+,-1,_')
                            self.update()
                        flag = False
                        self.h_val = data[h_offset + 1:]
                    else:
                        if flag:
                            # +,+,1
                            if l_offset > h_offset:
                                # lHhLl
                                self.l_val += data[:h_offset]
                                if self.h_val and self.l_val:
                                    print('+,+,1,lHhLl')
                                    self.update()
                                self.h_val = data[h_offset + 1:l_offset]
                                self.l_val = data[l_offset + 1:]
                            else:
                                # LlHh
                                self.l_val = data[l_offset + 1:h_offset]
                                if self.h_val and self.l_val:
                                    print('+,+,1,LlHh')
                                    self.update()
                                flag = False
                                self.h_val = data[h_offset + 1:]
                        else:
                            # +,+,0
                            if h_offset > l_offset:
                                # hLlHl
                                self.h_val += data[:l_offset]
                                self.l_val = data[l_offset + 1:h_offset]
                                if self.h_val and self.l_val:
                                    print('+,+,0,hLlHl')
                                    self.update()
                                self.h_val = data[h_offset + 1:]
                            else:
                                # HhLl
                                if self.h_val and self.l_val:
                                    print('+,+,0,HhLl')
                                    self.update()
                                self.h_val = data[h_offset + 1:l_offset]
                                flag = True
                                self.l_val = data[l_offset + 1:]
                del data
                # 保证秒数不同，数据库不会有重复的主键（时间）
                # sleep(1.5)
            # 提交事务并关闭数据库
            self.cur.close()
            self.conn.commit()
            self.conn.close()
            # 关闭socket
            self.master.socket.close()
        except OSError as e:
            print(e)
    
    def getValidData(self, _id):
        # print('getting valid data')
        # str 转 二维 list
        data = eval(self.h_val)
        # _id 时的线
        row = np.array(data[_id])
        # 化成mm
        row = np.true_divide(row, 100000)
        # 去除掉<0的无效数据
        row = row[row > 0]
        # 简单的滤波
        pre = 1
        flag = False
        for i in range(1, len(row)):
            dy1 = row[i] - row[i - 1]
            dy2 = row[i + 1] - row[i]
            if dy1 * pre < 0 and dy1 * dy2 < 0:
                row = np.delete(row, i)
                # i -= 1
            elif dy1 * dy2 < 0:
                if flag:
                    row = np.delete(row, i)
                else:
                    flag = True
                    pre = dy1
            else:
                pre = dy1
            if i + 2 >= len(row):
                break
        # 找到半圆
        n = len(row)
        self.row, self.col = [], []  # y,x
        self.start_y = None
        # 枚举中心的 x
        for center_x in range(n):
            self.start_y = row[center_x] - r
            self.center_x = center_x * 0.075
            l_cos = cos(radius(180 - r_angle))
            r_cos = cos(radius(l_angle))
            l_sin = sin(radius(180 - r_angle))
            r_sin = sin(radius(l_angle))
            l_dx = r * l_cos
            r_dx = r * r_cos
            left_x = self.center_x - l_dx
            right_x = self.center_x + r_dx
            self.l_id = int(left_x / 0.075)
            self.r_id = int(right_x / 0.075)
            if self.l_id < 0 or self.l_id >= n or self.r_id < 0 or self.r_id >= n:
                continue     
            left_y = row[self.l_id] - self.start_y
            right_y = row[self.r_id] - self.start_y
            left_true_height = r * l_sin
            right_true_height = r * r_sin
            valid1 = abs(left_y - left_true_height) < epos
            valid2 = abs(right_y - right_true_height) < epos
            if valid1 and valid2:
                self.row = row[self.l_id:self.r_id + 1]
                self.col = [i * 0.075 for i in range(self.l_id, self.r_id + 1)]
                break
        # self.col = [i for i in range(len(self.row))] # x轴
        # 计算实际面积
        self.area()
        # 磨损程度
        self.area_dif = abs(self.area_ideal - self.area_real).__round__(6)
        # 平均磨损深度
        self.height_avg = sqrt(2 * self.area_dif / pi)
        # 最大磨损深度
        self.height_max = 0
        # 最小磨损深度
        self.height_min = 1e9
        # self.height_sum = 0
        n = len(self.row)
        for i in range(n):
            x = self.col[i]
            real_y = self.row[i]
            ideal_y = sqrt(r ** 2 - abs(x - self.center_x))
            dif = abs(ideal_y + self.start_y - real_y)
            self.height_max = max(self.height_max, dif)
            self.height_min = min(self.height_min, dif)
            # self.height_sum += dif
        # self.height_sum /= n
        # 磨损程度
        self.result = ""
        if self.height_avg < 1.5:
            self.result = "无"
        elif 1.5 <= self.height_avg < 2.5:
             self.result = "小"
        elif 2.5 <= self.height_avg < 4:
             self.result = "中"
        elif 4 <= self.height_avg:
             self.result = "大"
        self.height_max = self.height_max.__round__(6)
        self.height_min = self.height_min.__round__(6)
        self.height_avg = self.height_avg.__round__(6)
        self.master.data_3d.append(self.row)

    def area(self):
        self.area_real = 0
        for i in range(len(self.row) - 1):
            self.area_real += (abs(self.row[i] - self.start_y) * 0.075)

    def toHTML(self):
        self.html = '<html><head><meta charset="utf-8" />'
        self.html += '<script src="https://cdn.plot.ly/plotly-latest.min.js"></script></head>'
        self.html += '<body>'
        self.html += po.plot(self.fig, include_plotlyjs=False,
                             output_type='div')
        self.html += '</body></html>'

    def update(self):
        now = getCurrentTime(3)
        # print('time:', now)
        # print('hval:', self.h_val)
        # print('lval:', self.l_val)
        # print('=================')
        # 存到数据库
        try:
            self.cur.execute("insert into data (time,h_val,l_val) values (\"%s\", \"%s\", \"%s\");" % (now, self.h_val, self.l_val))
        except sqlite3.OperationalError:
            # table不存在，创建
            self.cur.execute("""
                    create table data (
                        time text primary key not null,
                        h_val text not null,
                        l_val text not null
                    );
                """)
            self.cur.execute("insert into data (time,h_val,l_val) values (\"%s\", \"%s\", \"%s\");" % (now, self.h_val, self.l_val))
        # 处理数据
        _id = random.randint(2, 1000)
        print('start', _id)
        self.getValidData(_id)
        print('finish', _id)
        # 发送给窗体显示
        # self.fig = go.Figure(data=go.Scatter(x=self.col, y=self.row))
        trace0 = go.Scatter(x=self.col, y=self.row)
        x = [i for i in range(len(self.master.data_3d))]
        y = [i*0.075 for i in range(self.l_id, self.r_id+1)]
        trace1 = go.Surface(x=y, y=x, z=self.master.data_3d)
        self.fig = make_subplots(rows=2, cols=1,specs=[[{'is_3d': False}], [{'is_3d': True}]])
        self.fig.append_trace(trace0, 1, 1)  
        self.fig.append_trace(trace1, 2, 1)  
        self.toHTML()
        refresh()
        self.update_html.emit(self.html)
        self.update_time.emit(now)
        self.update_area_dif.emit(str(self.area_dif))
        self.update_area_res.emit(self.result)
        self.update_height_min.emit(str(self.height_min))
        self.update_height_avg.emit(str(self.height_avg))
        self.update_height_max.emit(str(self.height_max))
        # self.update_height.emit(self.h_val[0:max_size] + " ...")
        # self.update_luminance.emit(self.l_val[0:max_size] + " ...")
        self.master.master.statusbar.showMessage('已成功接收 %s 的数据' % now)
        refresh()


class CollectData(QWidget):
    """
    实时数据收集Tab
    开启Qthread通过socket接收数据
    """

    def __init__(self, master=None):
        QWidget.__init__(self, master)
        self.master = master
        self.collecting = False
        self.socket = socket.socket()
        self.socket.bind(addr)
        self.socket.listen(1)
        self.data_3d = []
        self.createUI()
        self.createSignalShot()

    def createUI(self):
        self.browser = QWebEngineView()
        time_label = QLabel("时间")
        self.time_value = QLabel(getCurrentTime(2))
        self.group = QGroupBox('实时数据')
        self.label1 = QLabel('磨损面积：')
        self.area_dif_val = QLabel()
        self.label2 = QLabel('磨损程度：')
        self.area_result = QLabel()
        self.label3 = QLabel('最小磨损深度：')
        self.height_min_val = QLabel()
        self.label4 = QLabel('平均磨损深度：')
        self.height_avg_val = QLabel()
        self.label5 = QLabel('最大磨损深度：')
        self.height_max_val = QLabel()
        grid = QGridLayout()
        grid.addWidget(time_label, 0, 0)
        grid.addWidget(self.time_value, 0, 1)
        grid.addWidget(self.label1, 1, 0)
        grid.addWidget(self.area_dif_val, 1, 1)
        grid.addWidget(self.label2, 2, 0)
        grid.addWidget(self.area_result, 2, 1)
        grid.addWidget(self.label3, 3, 0)
        grid.addWidget(self.height_min_val, 3, 1)
        grid.addWidget(self.label4, 4, 0)
        grid.addWidget(self.height_avg_val, 4, 1)
        grid.addWidget(self.label5, 5, 0)
        grid.addWidget(self.height_max_val, 5, 1)
        self.btn = QPushButton('开始收集')
        grid.addWidget(self.btn, 6, 0, 1, 2)
        self.group.setLayout(grid)
        hbox = QHBoxLayout()
        hbox.addWidget(self.group) 
        hbox.addWidget(self.browser)
        # 设置两个控件的比例
        hbox.setStretch(0, 1)
        hbox.setStretch(1, 5)
        self.setLayout(hbox)

    def createSignalShot(self):
        self.btn.clicked.connect(self.startCollect)

    def createBackendSignalShot(self):
        self.backend.update_time.connect(self.updateTime)
        # self.backend.update_height.connect(self.updateHeight)
        # self.backend.update_luminance.connect(self.updateLuminance)
        self.backend.update_html.connect(self.updateHTML)
        self.backend.update_area_dif.connect(self.updateAreaDif)
        self.backend.update_area_res.connect(self.updateAreaRes)
        self.backend.update_height_min.connect(self.updateHeightMin)
        self.backend.update_height_avg.connect(self.updateHeightAvg)
        self.backend.update_height_max.connect(self.updateHeightMax)

    def startCollect(self):
        if self.collecting:
            print('停止收集')
            self.master.statusbar.showMessage('停止收集')
            self.collecting = False
            self.backend.quit()
            self.btn.setText('开始收集') 
        else:
            print('开始收集')
            self.master.statusbar.showMessage('开始收集')
            self.btn.setText('结束收集')
            # 重新创建线程进行数据收集
            self.backend = BackendCollectData(self)
            self.createBackendSignalShot()
            self.backend.start()

    def updateTime(self, now):
        self.time_value.setText(now)

    # def updateHeight(self, height):
    #     refresh()
    #     self.height_value.setText(height)

    # def updateLuminance(self, luminance):
    #     refresh()
    #     self.luminance_value.setText(luminance)

    def updateHTML(self, html):
        self.browser.setHtml(html)

    def updateAreaDif(self, val):
        self.area_dif_val.setText(str(val))

    def updateAreaRes(self, val):
        self.area_result.setText(val)

    def updateHeightMin(self, val):
        self.height_min_val.setText(str(val))

    def updateHeightAvg(self, val):
        self.height_avg_val.setText(str(val))

    def updateHeightMax(self, val):
        self.height_max_val.setText(str(val))
    