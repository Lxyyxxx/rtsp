# -*- coding: utf-8 -*- 
#
# Created by Xinyu on 2020/8/1 23:11.
#

import datetime
import os
from time import sleep
import sqlite3
from PyQt5.QtWidgets import QApplication
from math import acos

# 路径
path_dir = os.path.dirname(os.path.abspath(__file__))
capture_path = os.path.join(path_dir, "capture")
db_path = os.path.join(path_dir, "database")
data_db_path = os.path.join(db_path, 'data.db')
video_path = os.path.join(path_dir, "video")
ffmpeg_path = os.path.join(path_dir, 'ffmpeg-4.3.1-win64-shared', 'bin')
icon_path = os.path.join(path_dir, 'icon')

# 如果不存在则创建
if not os.path.exists(capture_path):
    os.mkdir(capture_path)
if not os.path.exists(db_path):
    os.mkdir(db_path)
if not os.path.exists(video_path):
    os.mkdir(video_path)

# socket
# 缓存区大小，大小保证不会有HLHL这样的数据
buffer_size = 0xFFFFFF
# 地址
addr = ('127.0.0.1', 6666)
# 可显示的最大大小
max_size = 2048

# 数据库
# conn = sqlite3.connect(data_db_path)
# cur = conn.cursor()

# 数据关键词
keywords = ['time', 'h_val', 'l_val']
keymap = {'time': '时间', 'h_val': '高度', 'l_val': '亮度'}
keywords_cn = ['时间', '高度', '亮度']

# ffmpeg 录制线程
thread_num = 4

# 半径，左角度，右角度
r = 10
l_angle = 0
r_angle = 180
# 阈值
epos = 4
# 圆周率
pi = acos(-1)


def getCurrentTime(type):
    # 获得不同形式的当前时间
    if type == 1:
        return datetime.datetime.now().strftime('%Y_%m_%d_%H_%M_%S_%f')
    elif type == 2:
        return datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')
    elif type == 3:
        return datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S.%f')


def refresh():
    # 一边执行耗时程序，一边刷新
    QApplication.processEvents()
    sleep(0.1)


def ms2hms(mm):
    # minute,second 转 hour,minute,second
    m, s = divmod(mm / 1000.0, 60)
    h, m = divmod(m, 60)
    return "%02d:%02d:%02d" % (h, m, s)


def radius(angle):
    # 180 to 2pi
    return angle / 180 * (2 * pi)

# def createDataTable(cur):
#     # 创建存数据的数据库
#     cur.execute("""
#         create table data (
#             time text primary key not null,
#             h_val text not null,
#             l_val text not null
#         );
#     """)
#     print('create table successfully')
#     return cur
#
#
# def insertIntoDataTable(cur, now: str, h_val: str, l_val: str):
#     cur.execute("insert into data (time,h_val,l_val) values (\"%s\", \"%s\", \"%s\");" % (now, h_val, l_val))
#     print("insert record successfully")
#     return cur
#
#
# def queryFromDataTableAll(cur):
#     cur.execute("select * from data;")
#     print("query all record successfully")
#     return cur
#
#
# def queryFromDataTableWhereTime(cur, condition):
#     cur.execute("select * from data where time > %s;" % condition)
#     print("query record successfully")
#     return cur
