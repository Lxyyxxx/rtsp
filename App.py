# -*- coding: utf-8 -*- 
#
# Created by Xinyu on 2020/8/1 23:02.
#

import os
import sys
from PyQt5.QtWidgets import *
from PyQt5.QtGui import *
from PyQt5.QtCore import *
# import qtawesome as qta
from RealTimeMonitor import RealTimeMonitor
from ShowCapture import ShowCapture
# from CollectData import CollectData
from DrawData import CollectData
from ShowData import ShowData
from ShowVideo import ShowVideo
from function import refresh, icon_path


class App(QMainWindow):
    def __init__(self, master=None):
        QMainWindow.__init__(self, master)
        self.master = master
        self.url = None
        self.setWindowTitle("机器人视频流图传系统")
        self.setWindowIcon(QIcon(os.path.join(icon_path, "cam.ico")))
        # self.widget = QWidget(self)
        # self.setCentralWidget(self.widget)
        self.tab = QTabWidget(self)
        self.setCentralWidget(self.tab)
        self.statusbar = self.statusBar()
        self.first_click_tab3 = True
        # self.setCentralWidget(self.tab)
        self.createUI()
        self.createSignalSlot()
        # 自动开始捕获数据
        self.leftbar.startCollect()

    def getUrl(self, url):
        self.url = url
        self.tab0.open(self.url)

    def createUI(self):
        # Tab0
        self.tab0 = RealTimeMonitor(self)
        self.tab.addTab(self.tab0, '视频监控')
        # if self.url:
        #     self.tab0.open(self.url)
        # Tab1
        self.tab1 = ShowCapture(self)
        self.tab.addTab(self.tab1, '截图浏览')
        # Tab2
        self.tab2 = ShowData(self)
        self.tab.addTab(self.tab2, '数据报表')
        # Tab3
        self.tab3 = ShowVideo(self)
        self.tab.addTab(self.tab3, "监控回放")
        # left bar
        # group = QGroupBox('实时数据')
        # vbox = QVBoxLayout()
        self.leftbar = CollectData(self)
        # vbox.addWidget(self.leftbar)
        # group.setLayout(vbox)
        self.tab.addTab(self.leftbar, '实时数据')
        # # layout
        # # grid = QGridLayout(self)
        # # grid.addWidget(self.leftbar, 0, 0, 1, 2)
        # # grid.addWidget(group, 0, 0, 1, 2)
        # # grid.addWidget(self.tab, 0, 2, 1, 1)
        # # self.widget.setLayout(grid)
        # hbox = QHBoxLayout()
        # hbox.addWidget(group, 1)
        # hbox.addWidget(self.tab, 5)
        # # hbox.setStretch(0, 1)
        # # hbox.setStretch(1, 4)
        # self.widget.setLayout(hbox)
        # status bar
        self.statusbar.showMessage('这是状态提示栏')

    def createSignalSlot(self):
        self.tab.currentChanged.connect(self.tabClickChange)

    def tab3Hint(self):
        if self.first_click_tab3:
            # self.first_click_tab3 = False
            infobox = QMessageBox()
            infobox.setIcon(QMessageBox.Information)
            infobox.setText('请选择一个视频双击播放')
            self.statusbar.showMessage("请选择一个视频双击播放，若列表为空，则添加视频")
            infobox.setWindowTitle('选择视频')
            infobox.setStandardButtons(QMessageBox.Ok)
            infobox.button(QMessageBox.Ok).animateClick(3 * 1000)
            infobox.exec_()

    def tabClickChange(self):
        refresh()
        tab_id = self.tab.currentIndex()
        print('tab_id', tab_id)
        if tab_id == 1:
            self.tab1.getCapture()
        elif tab_id == 3:
            self.tab3Hint()
            self.tab3.getVideoList()

    def closeEvent(self, event):
        self.tab0.player.release()
        self.leftbar.socket.close()
        self.tab3.player.release()
        QCoreApplication.instance().quit()
        # sys.exit(app.exec_())


if __name__ == '__main__':
    app = QApplication(sys.argv)
    win = App()
    win.showMaximized()
    sys.exit(app.exec_())
