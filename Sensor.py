# -*- coding: utf-8 -*- 
#
# Created by Xinyu on 2020/8/1 23:02.
#

import os
import sys
from PyQt5.QtWidgets import *
from PyQt5.QtGui import *
from PyQt5.QtCore import *
# import qtawesome as qta
from RealTimeMonitor import RealTimeMonitor
from ShowCapture import ShowCapture
# from CollectData import CollectData
from DrawData import CollectData
from ShowData import ShowData
from ShowVideo import ShowVideo
from function import refresh, icon_path


class Sensor(QMainWindow):
    def __init__(self, master=None):
        QMainWindow.__init__(self, master)
        self.master = master
        self.setWindowTitle("机器人视频流图传系统")
        self.setWindowIcon(QIcon(os.path.join(icon_path, "cam.ico")))
        self.tab = QTabWidget(self)
        self.setCentralWidget(self.tab)
        self.statusbar = self.statusBar()
        self.createUI()
        # self.createSignalSlot()
        # 自动开始捕获数据
        self.tab0.startCollect()

    def getUrl(self, url):
        self.url = url
        self.tab0.open(self.url)

    def createUI(self):
        # Tab0
        self.tab0 = CollectData(self)
        self.tab.addTab(self.tab0, '实时数据')
        # Tab1
        self.tab1 = ShowData(self)
        self.tab.addTab(self.tab1, '数据报表')
        # status bar
        self.statusbar.showMessage('这是状态提示栏')

    # def createSignalSlot(self):
    #     self.tab.currentChanged.connect(self.tabClickChange)

    def closeEvent(self, event):
        self.tab0.socket.close()
        QCoreApplication.instance().quit()
        # sys.exit(app.exec_())


if __name__ == '__main__':
    app = QApplication(sys.argv)
    win = Sensor()
    win.showMaximized()
    sys.exit(app.exec_())
