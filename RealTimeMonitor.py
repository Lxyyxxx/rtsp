# -*- coding: utf-8 -*- 
#
# Created by Xinyu on 2020/8/1 23:03.
#

import os
import subprocess
from PyQt5.QtCore import *
from PyQt5.QtGui import *
from PyQt5.QtWidgets import *
import vlc
from function import capture_path, getCurrentTime, thread_num, video_path, ffmpeg_path


class RecordVideo:
    """
    利用ffmpeg进行视频录制
    """

    def __init__(self, master=None):
        self.master = master
        self.process = None

    def start(self, url=None, filename=None, shell=True):
        # ffmpeg -i rtsp://admin:1qa2ws3ed@192.168.88.111:554 -c:a copy -c:v copy -threads 4 -y op.mp4
        video_name = os.path.join(video_path, filename)
        cmd = "%s\\ffmpeg -i %s -c:a copy -c:v copy -threads %s -y %s" % (ffmpeg_path, url, thread_num, video_name)
        print(cmd)
        self.process = subprocess.Popen(cmd, shell=shell, universal_newlines=True, stdin=subprocess.PIPE,
                                        stderr=subprocess.STDOUT, stdout=subprocess.PIPE)

    def stop(self):
        # 按 q 退出
        self.process.stdin.write('q')
        self.process.communicate()

    def run(self):
        pass


class RealTimeMonitor(QMainWindow):
    """
    视频监控Tab
    根据rtsp url播放实时监控，暂停时可以截图，并显示截图，视频小窗播放
    """

    def __init__(self, master=None):
        QMainWindow.__init__(self, master)
        self.master = master
        # self.widget = QWidget(self)
        # self.setCentralWidget(self.widget)
        self.player = vlc.MediaPlayer()
        self.is_float = False
        self.is_recording = False
        self.rec = RecordVideo(self)
        self.createUI()
        self.createSignalSlot()

    def createUI(self):
        # 可悬浮播放器
        self.dock = QDockWidget(self)
        self.frame = QLabel()
        palette = self.frame.palette()
        palette.setColor(QPalette.Window, QColor(0, 0, 0))
        self.frame.setPalette(palette)
        self.frame.setAutoFillBackground(True)
        self.float_btn = QPushButton("开启小窗口")
        self.play_btn = QPushButton("暂停")
        self.cap_btn = QPushButton("视频截图")
        self.screen_cap_btn = QPushButton("屏幕截图")
        self.record_btn = QPushButton('开始录制')
        widget = QWidget()
        self.setCentralWidget(widget)
        hbox = QHBoxLayout()
        hbox.addWidget(self.play_btn)
        hbox.addWidget(self.cap_btn)
        hbox.addWidget(self.screen_cap_btn)
        hbox.addWidget(self.record_btn)
        vbox = QVBoxLayout()
        vbox.addWidget(self.frame)
        vbox.addWidget(self.float_btn)
        vbox.addLayout(hbox)
        widget.setLayout(vbox)
        self.dock.setWidget(widget)
        # self.dock.setWidget(self.frame)
        self.dock.setAllowedAreas(Qt.TopDockWidgetArea)
        self.dock.setFeatures(QDockWidget.NoDockWidgetFeatures)
        # self.dock.setBaseSize(960,720)
        self.addDockWidget(Qt.TopDockWidgetArea, self.dock)

        # # 其他控件
        # self.dock2 = QDockWidget(self)
        # self.label = QLabel("这里是控制栏")
        # self.dock2.setWidget(self.label)
        # self.dock2.setAllowedAreas(Qt.BottomDockWidgetArea)
        # self.dock2.setFeatures(QDockWidget.NoDockWidgetFeatures)
        # self.addDockWidget(Qt.BottomDockWidgetArea, self.dock2)

        # pic_name = 'qss/capture/2020_08_01_11_24_52_496672.png'
        # picture = QPixmap(pic_name)
        # self.frame.setPixmap(picture)

    def createSignalSlot(self):
        self.float_btn.clicked.connect(self.clickFloat)
        self.play_btn.clicked.connect(self.clickPlay)
        self.cap_btn.clicked.connect(self.clickCap)
        self.screen_cap_btn.clicked.connect(self.clickScreenCap)
        self.record_btn.clicked.connect(self.clickRecord)

    def clickFloat(self):
        # 悬浮窗口
        if not self.is_float:
            self.dock.setAllowedAreas(Qt.NoDockWidgetArea)  # 禁止停靠任何地方
            self.dock.setFloating(True)  # 打开悬浮状态
            self.is_float = True
            # 小窗口
            self.dock.resize(320, 240)
            self.float_btn.setText("关闭小窗口")
            self.master.statusbar.showMessage('已打开小窗口播放')
        else:
            self.dock.setAllowedAreas(Qt.TopDockWidgetArea)  # 停靠在顶端任何地方
            self.dock.setFloating(False)  # 关闭悬浮状态
            self.is_float = False
            self.float_btn.setText("开启小窗口")
            self.master.statusbar.showMessage('已关闭小窗口播放')

    def open(self, filename):
        # self.media = self.instance.media_new(filename)
        # self.player.set_media(self.media)
        self.player.set_mrl(filename)
        self.master.statusbar.showMessage('正在播放：%s' % filename)
        self.player.set_hwnd(self.frame.winId())
        # 不判断url是否有效了，无效就不播放，其他功能还能用
        # play 只要有链接，就会返回0，没有链接才会返回1
        self.player.play()
        # if not self.player.play():
        # print("视频播放失败")

    def clickPlay(self):
        if self.player.is_playing():
            self.player.pause()
            self.play_btn.setText("播放")
            self.master.statusbar.showMessage('视频被暂停')
        else:
            self.player.play()
            self.play_btn.setText("暂停")
            self.master.statusbar.showMessage('视频开始播放')

    def clickCap(self):
        filename = getCurrentTime(1) + ".png"
        # if not os.path.exists(capture_path):
        #     os.mkdir(capture_path)
        pic_name = os.path.join(capture_path, filename)
        # print(pic_name)
        # 通过 vlc 进行截图
        result = self.player.video_take_snapshot(0, pic_name, 0, 0)
        if result == 0:
            print("Suc")
            self.master.statusbar.showMessage('视频截图成功')
            # 刷新显示截图界面
            self.master.tab1.getCapture()
        else:
            print("Fail")
            self.master.statusbar.showMessage('视频截图失败')

    def clickScreenCap(self):
        filename = getCurrentTime(1) + ".png"
        pic_name = os.path.join(capture_path, filename)
        screen = QApplication.primaryScreen()
        result = screen.grabWindow(QApplication.desktop().winId())
        result.save(pic_name)
        self.master.statusbar.showMessage('屏幕截图成功')
        # 刷新显示截图界面
        self.master.tab1.getCapture()

    def clickRecord(self):
        filename = getCurrentTime(1) + ".mp4"
        if self.is_recording:
            self.rec.stop()
            self.is_recording = False
            # 刷新显示视频界面
            self.master.tab2.getVideoList()
            self.record_btn.setText('开始录制')
            self.master.statusbar.showMessage('已结束录制')
        else:
            self.is_recording = True
            self.rec.start(url=self.master.url, filename=filename)
            self.record_btn.setText('停止录制')
            self.master.statusbar.showMessage('视频已开始录制')
