# -*- coding: utf-8 -*- 
#
# Created by Xinyu on 2020/8/1 23:06.
#

import sqlite3
from PyQt5.QtCore import *
from PyQt5.QtWidgets import *
import socket
from function import refresh, getCurrentTime, addr, buffer_size, data_db_path, max_size
from time import sleep


class BackendCollectData(QThread):
    """
    后台收集数据
    """

    # 信号槽
    update_time = pyqtSignal(str)
    update_height = pyqtSignal(str)
    update_luminance = pyqtSignal(str)
    # 数据库
    conn = None
    cur = None

    def __init__(self, master=None):
        QThread.__init__(self, master)
        self.master = master

    def run(self):
        self.master.collecting = True
        try:
            self.master.master.statusbar.showMessage('正在等待socket连接')
            print('等待连接')
            client, _ = self.master.socket.accept()
            # 连接数据库，不存在会自行创建
            self.conn = sqlite3.connect(data_db_path)
            self.cur = self.conn.cursor()
            self.master.master.statusbar.showMessage('socket连接成功')
            print('连接成功')
            """
            开始切割数据
            h_offset l_offset flag state
            -1       -1       0     全是高度
            -1       +        0     l_offset前全为高度，后全为亮度
            -1       -1       1     全是亮度
            -1       +        1     x
            +        -1       0     x
            +        -1       1     h_offset前全为亮度，||，后全为高度
            +        +        0     l_offset前全为高度，l_offset到h_offset间全为亮度，||，h_offset后全为高度   hLlHl
            +        +        0     ||，h_offset到l_offset间全为高度，l_offset后全为亮度   HhLl
            +        +        1     h_offset前全为亮度，||，h_offset到l_offset间全为高度，l_offset后全为亮度   lHhLl
            +        +        1     l_offset到h_offset间全为亮度，||，h_offset后全为高度   LlHh
            """
            flag = False  # 0-高度还没读完，1-高度已读完
            self.h_val, self.l_val = "", ""
            while self.master.collecting:
                print('running...')
                refresh()
                data = client.recv(buffer_size).decode()
                # print(data)
                if not data:
                    # 更新最后一组数据
                    self.update()
                    # 提交事务，关闭数据库
                    self.cur.close()
                    self.conn.commit()
                    self.conn.close()
                    print('waiting...')
                    # 当data为空，说明客户端已断开连接，需要等待下次连接
                    client, _ = self.master.socket.accept()
                    # 重新连接数据库
                    self.conn = sqlite3.connect(data_db_path)
                    self.cur = self.conn.cursor()
                    continue
                # 找高度
                h_offset = data.find('H')
                # 找亮度
                l_offset = data.find('L')
                print(h_offset, l_offset, flag)
                if h_offset == -1:
                    if l_offset == -1:
                        # -1,-1,0
                        if not flag:
                            self.h_val += data
                        # -1,-1,1
                        else:
                            self.l_val += data
                    else:
                        # -1,+
                        self.h_val += data[:l_offset]
                        flag = True
                        self.l_val = ""
                        self.l_val += data[l_offset + 1:]
                else:
                    if l_offset == -1:
                        # +,-1
                        self.l_val += data[:h_offset]
                        if self.h_val and self.l_val:
                            print('+,-1,_')
                            self.update()
                        flag = False
                        self.h_val = data[h_offset + 1:]
                    else:
                        if flag:
                            # +,+,1
                            if l_offset > h_offset:
                                # lHhLl
                                self.l_val += data[:h_offset]
                                if self.h_val and self.l_val:
                                    print('+,+,1,lHhLl')
                                    self.update()
                                self.h_val = data[h_offset + 1:l_offset]
                                self.l_val = data[l_offset + 1:]
                            else:
                                # LlHh
                                self.l_val = data[l_offset + 1:h_offset]
                                if self.h_val and self.l_val:
                                    print('+,+,1,LlHh')
                                    self.update()
                                flag = False
                                self.h_val = data[h_offset + 1:]
                        else:
                            # +,+,0
                            if h_offset > l_offset:
                                # hLlHl
                                self.h_val += data[:l_offset]
                                self.l_val = data[l_offset + 1:h_offset]
                                if self.h_val and self.l_val:
                                    print('+,+,0,hLlHl')
                                    self.update()
                                self.h_val = data[h_offset + 1:]
                            else:
                                # HhLl
                                if self.h_val and self.l_val:
                                    print('+,+,0,HhLl')
                                    self.update()
                                self.h_val = data[h_offset + 1:l_offset]
                                flag = True
                                self.l_val = data[l_offset + 1:]
                del data
                # 保证秒数不同，数据库不会有重复的主键（时间）
                # sleep(1.5)
            # 提交事务并关闭数据库
            self.cur.close()
            self.conn.commit()
            self.conn.close()
            # 关闭socket
            self.master.socket.close()
        except OSError as e:
            print(e)

    def update(self):
        now = getCurrentTime(3)
        # print('time:', now)
        # print('hval:', self.h_val)
        # print('lval:', self.l_val)
        # print('=================')
        # 存到数据库
        try:
            self.cur.execute("insert into data (time,h_val,l_val) values (\"%s\", \"%s\", \"%s\");" % (now, self.h_val, self.l_val))
        except sqlite3.OperationalError:
            # table不存在，创建
            self.cur.execute("""
                    create table data (
                        time text primary key not null,
                        h_val text not null,
                        l_val text not null
                    );
                """)
            self.cur.execute("insert into data (time,h_val,l_val) values (\"%s\", \"%s\", \"%s\");" % (now, self.h_val, self.l_val))
        # 发送给窗体显示
        self.update_time.emit(now)
        self.update_height.emit(self.h_val[0:max_size] + " ...")
        self.update_luminance.emit(self.l_val[0:max_size] + " ...")
        self.master.master.statusbar.showMessage('已成功接收 %s 的数据' % now)


class CollectData(QWidget):
    """
    实时数据收集Tab
    开启Qthread通过socket接收数据
    """

    def __init__(self, master=None):
        QWidget.__init__(self, master)
        self.master = master
        self.collecting = False
        self.socket = socket.socket()
        self.socket.bind(addr)
        self.socket.listen(1)
        self.createUI()
        self.createSignalShot()

    def createUI(self):
        grid = QGridLayout()
        self.group = QGroupBox('实时信息')
        self.group.setLayout(grid)
        time_label = QLabel("时间")
        height_label = QLabel("高度")
        luminance_label = QLabel("亮度")
        self.time_value = QLabel(getCurrentTime(2))
        self.height_value = QTextEdit()
        self.luminance_value = QTextEdit()
        self.btn = QPushButton('开始收集')
        grid.addWidget(time_label, 0, 0)
        grid.addWidget(self.time_value, 1, 0)
        grid.addWidget(height_label, 2, 0)
        grid.addWidget(self.height_value, 3, 0)
        grid.addWidget(luminance_label, 4, 0)
        grid.addWidget(self.luminance_value, 5, 0)
        grid.addWidget(self.btn, 6, 0)
        self.setLayout(grid)

    def createSignalShot(self):
        self.btn.clicked.connect(self.startCollect)

    def createBackendSignalShot(self):
        self.backend.update_time.connect(self.updateTime)
        self.backend.update_height.connect(self.updateHeight)
        self.backend.update_luminance.connect(self.updateLuminance)

    def startCollect(self):
        if self.collecting:
            print('停止收集')
            self.master.statusbar.showMessage('停止收集')
            self.collecting = False
            self.backend.quit()
            self.btn.setText('开始收集')
        else:
            print('开始收集')
            self.master.statusbar.showMessage('开始收集')
            self.btn.setText('结束收集')
            # 重新创建线程进行数据收集
            self.backend = BackendCollectData(self)
            self.createBackendSignalShot()
            self.backend.start()

    def updateTime(self, now):
        refresh()
        self.time_value.setText(now)

    def updateHeight(self, height):
        refresh()
        self.height_value.setText(height)

    def updateLuminance(self, luminance):
        refresh()
        self.luminance_value.setText(luminance)
