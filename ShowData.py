# -*- coding: utf-8 -*- 
#
# Created by Xinyu on 2020/8/2 22:21.
#

import os
import sqlite3
from PyQt5.QtCore import *
from PyQt5.QtGui import *
from PyQt5.QtWidgets import *
from function import data_db_path, refresh, keywords, keymap, keywords_cn, max_size
import pandas as pd


class ShowData(QWidget):
    """
    数据报表Tab
    显示数据库中的数据，支持报表导出
    """
    def __init__(self, master=None):
        QWidget.__init__(self, master)
        self.master = master
        self.createUI()
        self.createSignalShot()

    def createUI(self):
        # btn
        group = QGroupBox('数据库操作')
        vbox = QVBoxLayout()
        self.query_btn = QPushButton('数据库查询')
        self.export_btn = QPushButton('报表导出')
        vbox.addWidget(self.query_btn)
        vbox.addWidget(self.export_btn)
        group.setLayout(vbox)
        # table
        self.table = QTableWidget()
        # 列数
        self.table.setColumnCount(3)
        # 所有列自动拉伸，充满界面
        self.table.horizontalHeader().setSectionResizeMode(QHeaderView.Stretch)
        # 只能选中一行
        self.table.setSelectionMode(QAbstractItemView.SingleSelection)
        # 不可编辑
        self.table.setEditTriggers(QTableView.NoEditTriggers)
        # 只有行选中
        self.table.setSelectionBehavior(QAbstractItemView.SelectRows)
        # 列宽高按照内容自适应
        self.table.resizeColumnsToContents()
        # 行宽高按照内容自适应
        self.table.resizeRowsToContents()
        self.table.setHorizontalHeaderLabels(keywords_cn)  # 标题
        # layout
        grid = QGridLayout()
        grid.addWidget(group, 0, 0)
        grid.addWidget(self.table, 0, 1)
        self.setLayout(grid)

    def createSignalShot(self):
        self.query_btn.clicked.connect(self.query)
        self.export_btn.clicked.connect(self.export)

    def query(self):
        if self.master.tab0.collecting:
            QMessageBox.warning(self, '警告', '请结束收集数据再查询！', QMessageBox.Yes)
            self.master.statusbar.showMessage('请结束收集数据再查询！')
        else:
            sql = "select * from data order by time desc;"
            conn = sqlite3.connect(data_db_path)
            cur = conn.cursor()
            cur.execute(sql)
            res = cur.fetchall()
            row, col = len(res), len(res[0])
            self.table.setRowCount(row)
            self.table.setColumnCount(col)
            for i in range(row):
                for j in range(col):
                    if j:
                        # 截断数据，防止窗体未响应
                        item = QTableWidgetItem(res[i][j][0:max_size])
                    else:
                        item = QTableWidgetItem(res[i][j])
                    self.table.setItem(i, j, item)
                refresh()
            cur.close()
            conn.close()

    def export(self):
        if self.master.tab0.collecting:
            QMessageBox.warning(self, '警告', '请结束收集数据再导出！', QMessageBox.Yes)
            self.master.statusbar.showMessage('请结束收集数据再导出！')
        else:
            sql = "select * from data order by time desc;"
            conn = sqlite3.connect(data_db_path)
            cur = conn.cursor()
            cur.execute(sql)
            self.data = [dict(zip(keywords, item)) for item in cur.fetchall()]
            cur.close()
            conn.close()
            self.save()

    def save(self):
        # print(self.data)
        filename, ok = QFileDialog.getSaveFileName(self, '文件保存', '', "*.xlsx;;All Files(*)")
        if not ok:
            print('保存文件失败')
            self.master.statusbar.showMessage('保存文件失败')
        else:
            df = pd.DataFrame(self.data)
            # print(df)
            df.rename(columns=keymap, inplace=True)
            writer = pd.ExcelWriter(filename)
            df.to_excel(writer, index=False, columns=keywords_cn)
            writer.save()
            print("保存文件成功")
            self.master.statusbar.showMessage('保存文件成功')
