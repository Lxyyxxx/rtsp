# -*- coding: utf-8 -*- 
#
# Created by Xinyu on 2020/8/3 22:24.
#

import os
from time import sleep
import vlc
from PyQt5.QtCore import *
from PyQt5.QtGui import *
from PyQt5.QtWidgets import *
from function import video_path, getCurrentTime, capture_path, refresh, ms2hms


class ShowVideo(QWidget):
    def __init__(self, master=None):
        QWidget.__init__(self, master)
        self.master = master
        self.player = vlc.MediaPlayer()
        self.video_list = []
        self.timer = QTimer(self)
        self.timer.setInterval(1000)
        self.createUI()
        self.createSignalShot()

    def createUI(self):
        # video list
        group = QGroupBox("播放列表")
        vbox2 = QVBoxLayout()
        self.list = QListWidget()
        self.list.setSelectionMode(QAbstractItemView.ExtendedSelection)
        self.list.setFrameShape(QListWidget.NoFrame)
        vbox2.addWidget(self.list)
        group.setLayout(vbox2)
        # vlc
        self.frame = QFrame()
        palette = self.frame.palette()
        palette.setColor(QPalette.Window, QColor(0, 0, 0))
        self.frame.setPalette(palette)
        self.frame.setAutoFillBackground(True)
        hbox1 = QHBoxLayout()
        hbox1.addWidget(self.frame)
        # hbox.addWidget(self.list)
        hbox1.addWidget(group)
        # btn
        self.add_btn = QPushButton("添加视频")
        self.remove_btn = QPushButton('删除视频')
        self.play_btn = QPushButton("播放")
        # self.stop_btn = QPushButton("停止")
        self.cap_btn = QPushButton("视频截图")
        self.screen_cap_btn = QPushButton("屏幕截图")
        hbox2 = QHBoxLayout()
        hbox2.addWidget(self.play_btn)
        # hbox2.addWidget(self.stop_btn)
        hbox2.addWidget(self.cap_btn)
        hbox2.addWidget(self.screen_cap_btn)
        hbox2.addStretch(2)
        hbox2.addWidget(self.add_btn)
        hbox2.addWidget(self.remove_btn)
        # 进度条
        self.slider = QSlider(Qt.Horizontal, self)
        self.slider.setToolTip("Position")
        self.slider.setMaximum(1000)
        self.start_time = QLabel("--:--")
        self.end_time = QLabel("--:--")
        hbox3 = QHBoxLayout()
        hbox3.addWidget(self.start_time)
        hbox3.addWidget(self.slider)
        hbox3.addWidget(self.end_time)
        # layout
        vbox1 = QVBoxLayout()
        vbox1.addLayout(hbox1)
        vbox1.addLayout(hbox3)
        vbox1.addLayout(hbox2)
        hbox1.setStretch(0, 4)
        hbox1.setStretch(1, 1)
        self.setLayout(vbox1)

    def createSignalShot(self):
        self.slider.sliderMoved.connect(self.clickSlider)
        self.play_btn.clicked.connect(self.clickPlay)
        self.cap_btn.clicked.connect(self.clickCap)
        self.screen_cap_btn.clicked.connect(self.clickScreenCap)
        self.add_btn.clicked.connect(self.clickAdd)
        self.remove_btn.clicked.connect(self.clickRemove)
        self.list.itemDoubleClicked.connect(self.clickList)
        self.timer.timeout.connect(self.updateTime)

    def getVideoList(self):
        video_dir = QDir(video_path)
        video_dir.setFilter(QDir.Files | QDir.NoSymLinks)
        filters = ["*.mp4"]
        video_dir.setNameFilters(filters)
        self.video_list = video_dir.entryList()
        # 新的在最前面
        sorted_list = sorted(
            self.video_list,
            key=lambda x: os.path.getmtime(os.path.join(video_path, x)),
            reverse=True
        )
        self.list.clear()
        self.list.addItems(sorted_list)
        refresh()

    def clickSlider(self, position):
        self.player.set_position(position / 1000.0)

    def clickPlay(self):
        if self.player.is_playing():
            self.player.pause()
            self.timer.stop()
            self.play_btn.setText("播放")
        else:
            if self.master.first_click_tab2:
                self.master.tab2Hint()
            else:
                self.timer.start()
                # sleep(1)
                self.player.play()
                self.play_btn.setText("暂停")

    def clickCap(self):
        filename = getCurrentTime(1) + ".png"
        pic_name = os.path.join(capture_path, filename)
        # 通过 vlc 进行截图
        result = self.player.video_take_snapshot(0, pic_name, 0, 0)
        if result == 0:
            print("Suc")
            self.master.statusbar.showMessage('视频截图成功')
            # 刷新显示截图界面
            self.master.tab1.getCapture()
        else:
            print("Fail")
            self.master.statusbar.showMessage('视频截图失败')

    def clickScreenCap(self):
        filename = getCurrentTime(1) + ".png"
        pic_name = os.path.join(capture_path, filename)
        screen = QApplication.primaryScreen()
        result = screen.grabWindow(QApplication.desktop().winId())
        result.save(pic_name)
        self.master.statusbar.showMessage('屏幕截图成功')
        # 刷新显示截图界面
        self.master.tab1.getCapture()

    def clickAdd(self):
        filename, ok = QFileDialog.getOpenFileNames(self, '选择文件', '', "*.mp4;;*.avi;;All Files(*)")
        if ok:
            self.video_list += filename
            # print(self.video_list, filename)
            sorted_list = sorted(
                self.video_list,
                key=lambda x: os.path.getmtime(os.path.join(video_path, x)),
                reverse=True
            )
            self.list.clear()
            self.list.addItems(sorted_list)
            refresh()

    def clickRemove(self):
        del_items = self.list.selectedItems()
        if del_items:
            for del_item in del_items:
                self.list.takeItem(self.list.row(del_item))
                self.video_list.remove(del_item.text())
                del del_item
        else:
            infobox = QMessageBox()
            infobox.setIcon(QMessageBox.Information)
            infobox.setText('请选择需要移除的视频')
            self.master.statusbar.showMessage("请选择需要移除的视频，可以多选")
            infobox.setWindowTitle('移除视频')
            infobox.setStandardButtons(QMessageBox.Ok)
            infobox.button(QMessageBox.Ok).animateClick(3 * 1000)
            infobox.exec_()

    def clickList(self, item):
        filename = item.text()
        if filename.find(':') == -1:
            # 没有盘符，代表是video_path中的视频，加上路径
            filename = os.path.join(video_path, filename)
        # print(filename)
        self.player.set_mrl(filename)
        self.master.statusbar.showMessage('正在播放：%s' % filename)
        self.player.set_hwnd(self.frame.winId())
        # 开始播放
        self.master.first_click_tab2 = False
        self.clickPlay()

    def updateTime(self):
        duration, current = ms2hms(self.player.get_length()), ms2hms(self.player.get_time())
        self.start_time.setText(current)
        self.end_time.setText(duration)
        self.slider.setValue(self.player.get_position() * 1000)
        # print(self.player.get_state())
        # Todo 有的时候结束了会造成程序未响应
        if self.player.get_state() == vlc.State.Ended:
            self.timer.stop()
            self.player.stop()
            self.play_btn.setText("播放")
            infobox = QMessageBox()
            infobox.setIcon(QMessageBox.Information)
            infobox.setText('视频播放结束')
            self.master.statusbar.showMessage('视频播放结束')
            infobox.setWindowTitle('提示')
            infobox.setStandardButtons(QMessageBox.Ok)
            infobox.button(QMessageBox.Ok).animateClick(3 * 1000)
            infobox.exec_()
